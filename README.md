# This is a simple file organiser.
## Simply run the script, choose the file desination and let it do it's thing.
### There are a few bugs at the moment and if you find one, please let me know

# How to run

1.  Download or clone repo
2.  If you want to only use this for the current directory that the file is in, use same_folder.py. This has significantly less bugs than multi_folder.py
3.  If you want to be able to use this anywhere, click on multi_folder.py. This will prompt you to choose where you want the organisation to happen.
